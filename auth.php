<?php

    header('Content-Type: application/json');
    session_start();
    include 'cors.php';
    require_once("func_util.php");
    $database = connectDB("gsb_frais", $config);
    $post_json_data = (array) json_decode(stripslashes(file_get_contents("php://input")));

  
        if(isset($post_json_data)){
            $post_data = array(["username", 50], ["password", 255]);
            $post_data = post_security($post_data);

            if(!$error){
                $sqlr = $database->prepare("SELECT `login`, `mdp`, `id`, `prenom`, `nom`,`comptable` FROM visiteur WHERE login = :username");
                $sqlr->bindParam(':username', $post_data["username"]);
                $sqlr->execute();
                $sqlr_rows = $sqlr->fetchAll();
        
                if (!empty($sqlr_rows)) {
                    if($post_data["password"] == $sqlr_rows[0]["mdp"]){
//                        if( $sqlr_rows[0]["isActive"] == 1){
                        $return_data = [
                            "id" => 1,
                            "message" => "Good password",
                            "user_id" => $sqlr_rows[0]["id"],// ici on peut rajouter toutes les infos qu'on veut récup du user
                            "user_prenom" =>$sqlr_rows[0]["prenom"],
                            "user_nom" =>$sqlr_rows[0]["nom"],
                            "user_comptable" =>$sqlr_rows[0]["comptable"],
//                            "user_isActive" =>$sqlr_rows[0]["isActive"],


                            "sessionId" => session_id()
                        ];
                        $_SESSION["id"] = $sqlr_rows[0]["id"];
                        $_SESSION["username"] = $sqlr_rows[0]["login"];
                        $_SESSION["prenom"] = $sqlr_rows[0]["prenom"];
                        $_SESSION["comptable"] = $sqlr_rows[0]["comptable"];
/*                         $_SESSION["isActive"] = $sqlr_rows[0]["isActive"];
                      } else {
                        $return_data = [
                            "id" => 3,
                            "message" => "Inactive"
                        ];
                      } */
                    }else{
                        $return_data = [
                            "id" => 2,
                            "message" => "Bad password"
                        ];
                    }
                }else{
                    $return_data = [
                        "id" => 2,
                        "message" => "Bad user"
                    ];
                }
            }
        }
    

    echo json_encode($return_data);