<?php
include 'cors.php';
require 'connect.php';

$sessionId = isset(getallheaders()["PHPSESSID"]) ? getallheaders()["PHPSESSID"]: null;

if($sessionId) {
  session_id(($sessionId));
}
session_start();

if(!isset($_SESSION["id"])) {
  http_response_code(403);
  exit;
}
$idUtilisateur = $_SESSION["id"];

$mois = $_GET['mois'];
$idVisiteur = $_GET['idVisiteur'];
$fichefrais = [];
$forfait =  [];
$horsForfait =  [];

$fiche = "SELECT * FROM fichefrais 
INNER JOIN etat ON fichefrais.idEtat = etat.id
WHERE idVisiteur='$idVisiteur' AND mois='$mois'";
if($result = mysqli_query($con,$fiche))
{
  while($row = mysqli_fetch_assoc($result))
  {
    $fichefrais['idVisiteur']    = $row['idVisiteur'];
    $fichefrais['mois'] = $row['mois'];
    $fichefrais['nbJustificatifs'] = $row['nbJustificatifs'];
    $fichefrais['montantValide']    = $row['montantValide'];
    $fichefrais['dateModif'] = $row['dateModif'];
    $fichefrais['idEtat'] = $row['idEtat'];
    $fichefrais['libelle'] = $row['libelle'];
  }   
}
else
{
  http_response_code(404);
}

$fraisForfait = "SELECT * FROM lignefraisforfait INNER JOIN fraisforfait 
ON lignefraisforfait.idFraisForfait = fraisforfait.id
WHERE idVisiteur='$idVisiteur' AND mois='$mois'";

if($result = mysqli_query($con,$fraisForfait))
{
  $cr = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $forfait[$row['idFraisForfait']] = array(
      'quantite' => $row['quantite'],
      "montant_unitaire" => $row['montant'],
      "total" => $row['quantite'] * $row['montant']
    );
  }    
}
else
{
  http_response_code(404);
}

$fraisHorsForfait = "SELECT * FROM lignefraishorsforfait 
WHERE idVisiteur='$idVisiteur' AND mois='$mois'";
if($result = mysqli_query($con,$fraisHorsForfait))
{
  while($row = mysqli_fetch_assoc($result))
  {
    $horsForfait['idVisiteur']    = $row['idVisiteur'];
    $horsForfait['mois'] = $row['mois'];
    $horsForfait['libelle'] = $row['libelle'];
    $horsForfait['date']    = $row['date'];
    $horsForfait['montant'] = $row['montant'];
  }    
}
else
{
  http_response_code(404);
}


if(empty($fichefrais)) {
  $fichefrais = null;
}

if(empty($forfait)) {
  $forfait = null;
}

if(empty($horsForfait)) {
  $horsForfait = null;
}

echo(json_encode(array("ficheFrais"=>$fichefrais, "forfait"=> $forfait, "horsForfait"=>$horsForfait)));