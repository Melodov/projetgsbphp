<?php
include 'cors.php';
require 'connect.php';

$sessionId = isset(getallheaders()["PHPSESSID"]) ? getallheaders()["PHPSESSID"]: null;

if($sessionId) {
  session_id(($sessionId));
}
session_start();


if(!isset($_SESSION["id"])) {
  http_response_code(403);
  exit;
}
$idUtilisateur = $_SESSION["id"];
$fichefrais = [];
$sql = "SELECT idVisiteur, mois, 
nbJustificatifs, montantValide, dateModif, idEtat, libelle FROM 
fichefrais INNER JOIN etat ON fichefrais.idEtat = etat.id WHERE idVisiteur='$idUtilisateur'
ORDER BY mois DESC";

if($result = mysqli_query($con,$sql))
{
  $cr = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $fichefrais[$cr]['idVisiteur']    = $row['idVisiteur'];
    $fichefrais[$cr]['mois'] = $row['mois'];
    $fichefrais[$cr]['nbJustificatifs'] = $row['nbJustificatifs'];
    $fichefrais[$cr]['montantValide']    = $row['montantValide'];
    $fichefrais[$cr]['dateModif'] = $row['dateModif'];
    $fichefrais[$cr]['idEtat'] = $row['idEtat'];
    $fichefrais[$cr]['libelle'] = $row['libelle'];

    $cr++;
  }
    
  echo json_encode(['data'=>$fichefrais]);
}
else
{
  http_response_code(404);
}