<?php
 
include 'cors.php';
require 'connect.php';

$mois = $_GET['mois'];
$idVisiteur = $_GET['idVisiteur'];

$sqls = array(
  "horsforfait" => "DELETE FROM lignefraishorsforfait WHERE idVisiteur='$idVisiteur' AND mois='$mois'",
  "fraisforfait" => "DELETE FROM lignefraisforfait WHERE idVisiteur='$idVisiteur' AND mois='$mois'",
  "fiche" => "DELETE FROM fichefrais WHERE idVisiteur='$idVisiteur' AND mois='$mois'",
);

$results = array();

foreach($sqls as $elementType => $sql) {
  if($result = mysqli_query($con,$sql))
  {
    $results[$elementType] = $result;
    echo json_encode($results);
  }
  else
  {
    $results = array('error' => mysqli_error($con), 'sql'=>$sql);
    http_response_code(400);
    break;
  }
}
