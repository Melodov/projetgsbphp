<?php
//header('Content-Type: application/json; charset=utf-8');

include 'cors.php';
require 'connect.php';

$mois = $_POST['mois'];
$idVisiteur = $_POST['idVisiteur'];
$qteEtape = $_POST['qteEtape'];
$qteKilometre = $_POST['qteKilometre'];
$qteNuit = $_POST['qteNuit'];
$qteRepas = $_POST['qteRepas'];

$sqls = array(
  "delete" => "DELETE FROM lignefraisforfait WHERE idVisiteur='$idVisiteur' AND mois='$mois'",
  "ETP" => "INSERT INTO lignefraisforfait (idVisiteur, mois, idFraisForfait, quantite) VALUES ('$idVisiteur', '$mois', 'ETP', '$qteEtape')",
  "KM" => "INSERT INTO lignefraisforfait (idVisiteur, mois, idFraisForfait, quantite) VALUES ('$idVisiteur', '$mois', 'KM', '$qteKilometre')",
  "NUI" => "INSERT INTO lignefraisforfait (idVisiteur, mois, idFraisForfait, quantite) VALUES ('$idVisiteur', '$mois', 'NUI', '$qteNuit')",
  "REP" => "INSERT INTO lignefraisforfait (idVisiteur, mois, idFraisForfait, quantite) VALUES ('$idVisiteur', '$mois', 'REP', '$qteRepas')",
);

$results = array();

foreach($sqls as $elementType => $sql) {
  if($result = mysqli_query($con,$sql))
  {
    $results[$elementType] = $result;
  }
  else
  {
    $results = array('error' => mysqli_error($con), 'sql'=>$sql);
    http_response_code(400);
    break;
  }
}
echo json_encode($results);