<?php
/**
 * Returns the list of cars.
 */

header("Access-Control-Allow-Origin: *");
require 'connect.php';
    
$visiteur = [];
$sql = "SELECT id, nom, prenom FROM visiteur";

if($result = mysqli_query($con,$sql))
{
  $cr = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $visiteur[$cr]['id']    = $row['id'];
    $visiteur[$cr]['prenom'] = $row['prenom'];
    $visiteur[$cr]['nom'] = $row['nom'];
    $cr++;
  }
    
  echo json_encode(['data'=>$visiteur]);
}
else
{
  http_response_code(404);
}