<?php

// Initialisation de la session.
// Si vous utilisez un autre nom
// session_name("autrenom")
session_start();
include 'cors.php';
require_once("func_util.php");
$database = connectDB("gsb_frais", $config);
// Détruit toutes les variables de session


session_destroy();
echo json_encode(array("success"=>true));

?>