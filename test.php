<?php
/**
 * Returns the list of cars.
 */

header("Access-Control-Allow-Origin: *");
require 'connect.php';
    
$fichefrais = [];
$sql = "SELECT idVisiteur, mois, nbJustificatifs, montantValide, dateModif, idEtat FROM fichefrais";

if($result = mysqli_query($con,$sql))
{
  $cr = 0;
  while($row = mysqli_fetch_assoc($result))
  {
    $fichefrais[$cr]['idVisiteur']    = $row['idVisiteur'];
    $fichefrais[$cr]['mois'] = $row['mois'];
    $fichefrais[$cr]['nbJustificatifs'] = $row['nbJustificatifs'];
    $fichefrais[$cr]['montantValide']    = $row['montantValide'];
    $fichefrais[$cr]['dateModif'] = $row['dateModif'];
    $fichefrais[$cr]['idEtat'] = $row['idEtat'];
    $cr++;
  }
    
  echo json_encode(['data'=>$fichefrais]);
}
else
{
  http_response_code(404);
}

  echo "Hello world!"; // Your code here
